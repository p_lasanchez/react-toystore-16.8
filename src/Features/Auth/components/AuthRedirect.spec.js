import React from 'react'
import { MemoryRouter, Redirect } from 'react-router-dom'
import TestRenderer, { act } from 'react-test-renderer'
import AuthRedirect from './AuthRedirect'

describe('<AuthRedirect />', () => {
  it('should render', () => {
    const close = jest.fn()
    let testRenderer

    act(() => {
      testRenderer = TestRenderer.create(
        <MemoryRouter initialEntries={['/']}>
          <AuthRedirect close={close} />
        </MemoryRouter>,
      )
    })

    const instance = testRenderer.root
    const el = instance.findByType(Redirect)
    expect(el.props.to).toBe('/basket')
    expect(close).toBeCalled()

    testRenderer.unmount()
  })
})
