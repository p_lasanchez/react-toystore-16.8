import React from 'react'
import { Provider } from 'react-redux'
import { ThemeProvider } from 'styled-components'

import store from './Store/store'
import Main from './Features/Main'
import theme from './theme'
import GlobalStyle from './GlobalStyle'

const App = () => (
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <React.Fragment>
        <GlobalStyle />
        <Main />
      </React.Fragment>
    </ThemeProvider>
  </Provider>
)

export default App
