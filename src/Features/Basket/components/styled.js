import styled from 'styled-components'

export const StyledBasketWrapper = styled.section`
  display: flex;
  flex-wrap: nowrap;
  justify-content: space-between;
  margin: 2rem 2rem 0;
  width: calc(100% - 4rem);
`

export const StyledToyColumn = styled.article`
  width: 48%;
`

export const StyledToyWrapper = styled.button`
  align-items: center;
  background: none;
  border-color: ${props => props.theme.colors.white};
  border-style: solid;
  border-width: 1px;
  color: ${props => props.theme.colors.white};
  cursor: pointer;
  display: flex;
  flex-wrap: wrap;
  font-size: 1rem;
  justify-content: space-between;
  margin: 0 0 2rem;
  padding: 1rem;
  width: 100%;
`

export const StyledToyText = styled.span`
color: ${props => props.theme.colors.white};
font-size: 1rem;
&::first-letter {
  text-transform: uppercase;
}
`

export const StyledPrice = styled.article`
  border-color: ${props => props.theme.colors.white};
  border-style: solid;
  border-width: 1px;
  color: ${props => props.theme.colors.white};
  font-size: 1.5rem;
  padding: 2rem;
  text-align: right;
`
