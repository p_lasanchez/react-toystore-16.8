import { act } from 'react-test-renderer'
import { mountWithStore } from '../../testUtils/utils.spec'

import { defaultAuthState } from '../Store/authReducer/auth.reducer'
import { defaultToyListState } from '../Store/toyListReducer/toyList.reducer'

import Main from './Main'
import Auth from './Auth/Auth'

jest.mock('axios', () => ({
  get: () => Promise.resolve('done'),
}))

describe('<Main />', () => {
  let store
  let renderer

  afterEach(() => {
    renderer.unmount()
    store.clearActions()
  })

  function initRenderer({ isOpened }) {
    const { _renderer, _store } = mountWithStore({
      authReducer: { ...defaultAuthState, isOpened },
      toyListReducer: { ...defaultToyListState },
    }, Main)

    renderer = _renderer
    store = _store
  }

  it('should render with auth', () => {
    act(() => initRenderer({ isOpened: true }))

    const instance = renderer.root
    const auth = instance.findByType(Auth)
    expect(auth).toBeDefined()
  })

  it('should render without auth', () => {
    act(() => initRenderer({ isOpened: false }))

    const instance = renderer.root
    const auth = instance.findAllByType(Auth)
    expect(auth.length).toBe(0)
  })
})
