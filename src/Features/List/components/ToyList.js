import React from 'react'
import PropTypes from 'prop-types'
import { StyledToyWrapper } from './styled'

import Toy from './Toy'
import types from '../models/toyType'

const propTypes = {
  toys: PropTypes.arrayOf(PropTypes.shape(types.toyType)).isRequired,
  handleClick: PropTypes.func.isRequired,
}

const ToyList = ({ handleClick, toys }) => (
  <StyledToyWrapper>
    {
      toys.map(toy => <Toy key={toy.id} handleClick={handleClick} {...toy} />)
    }
  </StyledToyWrapper>
)


ToyList.propTypes = propTypes

export default ToyList
