import toyListReducer, { defaultToyListState } from './toyList.reducer'
import { TOYS } from './toyList.actions'

describe('toyListReducer', () => {
  it('should return default state', () => {
    const result = toyListReducer({ ...defaultToyListState }, {
      type: 'fake',
    })

    expect(result).toEqual(defaultToyListState)
  })

  it('should return toys', () => {
    const result = toyListReducer({ ...defaultToyListState }, {
      type: TOYS.GET_TOYS_SUCCESS,
      toys: ['balloon'],
    })

    expect(result).toEqual({
      ...defaultToyListState,
      toys: ['balloon'],
      isLoaded: true,
    })
  })

  it('should select toy', () => {
    const fakeState = {
      ...defaultToyListState,
      toys: [{ id: 3 }, { id: 4 }],
    }
    const result = toyListReducer(fakeState, {
      type: TOYS.SELECT_TOY,
      id: 3,
    })

    expect(result).toEqual({
      ...defaultToyListState,
      toys: [{ id: 3, selected: true }, { id: 4 }],
    })
  })
})
