import { AUTH } from './auth.actions'

export const defaultAuthState = {
  isConnected: false,
  isOpened: false,
  isInvalid: false,
}

const authReducer = (state = defaultAuthState, action) => {
  switch (action.type) {
    case AUTH.OPEN:
      return { ...state, isOpened: true, isInvalid: false }

    case AUTH.CLOSE:
      return { ...state, isOpened: false }

    case AUTH.CONNECT:
      return { ...state, isConnected: true }

    case AUTH.CONNECT_ERROR:
      return { ...state, isInvalid: true }

    default:
      return state
  }
}

export default authReducer
