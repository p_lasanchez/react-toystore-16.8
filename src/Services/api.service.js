import axios from 'axios'
import config from '../config'

const instance = axios.create({
  baseURL: config.REACT_APP_BASE_URL,
  timeout: 1000,
  headers: {
    Auth: 'not connected',
  },
})

export const getToys = () => instance.get('/toys').then((response) => {
  instance.defaults.headers.common.Auth = 'my token'
  return response
})

export const authentication = (user, pass) => (
  instance.get(`/users?user=${user}&pass=${pass}`)
)
