import React from 'react'
import TestRenderer, { act } from 'react-test-renderer'
import { ThemeProvider } from 'styled-components'
import theme from '../../../theme'

import BasketPrice from './BasketPrice'
import { StyledPrice } from './styled'

describe('<BasketPrice />', () => {
  it('should render', () => {
    let renderer
    act(() => {
      renderer = TestRenderer.create(
        <ThemeProvider theme={theme}>
          <BasketPrice
            price={20}
          />
        </ThemeProvider>,
      )
    })

    const instance = renderer.root
    const content = instance.findByType(StyledPrice)

    expect(content.props.children).toEqual([20, '€'])
  })
})
