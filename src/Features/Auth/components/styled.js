import styled from 'styled-components'

export const AuthBackground = styled.aside`
  align-items: center;
  background: ${props => props.theme.colors.opaque};
  display: flex;
  height: 100%;
  justify-content: center;
  left: 0;
  position: fixed;
  top: 0;
  width: 100%;
`

export const AuthBox = styled.section`
  border: 1px solid ${props => props.theme.colors.white};
  background: ${props => props.theme.colors.black};
  padding: 1rem 3rem;
  text-align: center;
  width: 20rem;
`

export const AuthTitle = styled.h2`
  color: ${props => props.theme.colors.white};
  font-size: 1rem;
  padding: 0 0 1rem;
  text-align: center;
  text-transform: uppercase;
`

const setInvalidColor = props => (
  props.isInvalid ? props.theme.colors.red : props.theme.colors.white
)

export const AuthInput = styled.input`
  background: transparent !important;
  border-style: solid;
  border-color: ${props => setInvalidColor(props)};
  border-width: 0 0 1px;
  color: ${props => setInvalidColor(props)} !important;
  display: block;
  font-size: 1rem;
  margin-bottom: 2rem;
  padding: 0 0 0.5rem;
  outline: none;
  text-align: left;
  width: 100%;
  &:last-of-type {
    margin: 0;
  }
`

export const AuthMessage = styled.p`
  color: ${props => props.theme.colors.red};
  opacity: ${props => (props.isVisible ? 1 : 0)};
  font-weight: bold;
  padding: 0.7rem 0;
`

export const AuthButton = styled.button`
  background-color: transparent;
  border: 1px solid ${props => props.theme.colors.white};
  color: ${props => props.theme.colors.white};
  cursor: pointer;
  font-size: 1rem;
  outline: none;
  padding: 0.5rem 2rem;
  text-align: center;
  &:hover {
    opacity: 0.5;
  }
`
