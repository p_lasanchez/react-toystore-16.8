import React, { useState, useRef, useEffect } from 'react'
import PropTypes from 'prop-types'

import {
  AuthBackground, AuthBox, AuthInput, AuthMessage, AuthButton, AuthTitle,
} from './styled'

const propTypes = {
  close: PropTypes.func.isRequired,
  submit: PropTypes.func.isRequired,
  isInvalid: PropTypes.bool.isRequired,
}
const AuthForm = ({ close, submit, isInvalid }) => {
  const [email, setEmail] = useState('')
  const [pass, setPass] = useState('')
  const emailField = useRef(null)

  useEffect(() => {
    emailField.current.focus()
  }, [])

  const handleChangeMail = (e) => {
    setEmail(e.target.value)
  }

  const handleChangePass = (e) => {
    setPass(e.target.value)
  }

  const prevent = (e) => {
    e.preventDefault()
    e.stopPropagation()
  }

  const handleSubmit = () => {
    submit(email, pass)
  }

  return (
    <AuthBackground onClick={close}>
      <AuthBox onClick={prevent}>
        <AuthTitle>Identification</AuthTitle>
        <form noValidate>
          <AuthInput
            type="email"
            isInvalid={isInvalid}
            value={email}
            onChange={handleChangeMail}
            ref={emailField}
          />
          <AuthInput type="password" isInvalid={isInvalid} value={pass} onChange={handleChangePass} />
          <AuthMessage isVisible={isInvalid}>Identifiants invalides</AuthMessage>
          <AuthButton onClick={handleSubmit}>Connect</AuthButton>
        </form>
      </AuthBox>
    </AuthBackground>
  )
}

AuthForm.propTypes = propTypes

export default React.memo(AuthForm, (prev, next) => prev.isInvalid === next.isInvalid)
