const dev = {
  REACT_APP_BASE_URL: 'http://localhost:9000',
}

const prod = {
  REACT_APP_BASE_URL: 'https://google.fr',
}

const config = process.env.REACT_APP_STAGE === 'dev' ? dev : prod

export default config
