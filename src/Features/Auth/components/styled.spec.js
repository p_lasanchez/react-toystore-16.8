import React from 'react'
import TestRenderer from 'react-test-renderer'
import theme from '../../../theme'
import 'jest-styled-components'

import {
  AuthBackground, AuthBox, AuthTitle, AuthInput, AuthMessage, AuthButton,
} from './styled'

describe('Auth uiComponents', () => {
  it('should mount AuthBackground', () => {
    const testRenderer = TestRenderer.create(
      <AuthBackground theme={theme} />,
    )

    const el = testRenderer.toJSON()
    expect(el).toHaveStyleRule('background', 'rgba(0,0,0,0.4)')
  })

  it('should mount AuthBox', () => {
    const testRenderer = TestRenderer.create(
      <AuthBox theme={theme} />,
    )

    const el = testRenderer.toJSON()
    expect(el).toHaveStyleRule('border', '1px solid #FFF')
    expect(el).toHaveStyleRule('background', theme.colors.black)
  })

  it('should mount AuthTitle', () => {
    const testRenderer = TestRenderer.create(
      <AuthTitle theme={theme} />,
    )

    const el = testRenderer.toJSON()
    expect(el).toHaveStyleRule('color', theme.colors.white)
  })

  it('should mount AuthInput invalid', () => {
    const testRenderer = TestRenderer.create(
      <AuthInput theme={theme} isInvalid />,
    )

    const el = testRenderer.toJSON()
    expect(el).toHaveStyleRule('border-color', theme.colors.red)
    expect(el).toHaveStyleRule('color', `${theme.colors.red} !important`)
  })

  it('should mount AuthInput', () => {
    const testRenderer = TestRenderer.create(
      <AuthInput theme={theme} />,
    )

    const el = testRenderer.toJSON()
    expect(el).toHaveStyleRule('border-color', theme.colors.white)
    expect(el).toHaveStyleRule('color', `${theme.colors.white} !important`)
  })

  it('should mount AuthMessage visible', () => {
    const testRenderer = TestRenderer.create(
      <AuthMessage theme={theme} isVisible />,
    )

    const el = testRenderer.toJSON()
    expect(el).toHaveStyleRule('color', theme.colors.red)
    expect(el).toHaveStyleRule('opacity', '1')
  })

  it('should mount AuthMessage invisible', () => {
    const testRenderer = TestRenderer.create(
      <AuthMessage theme={theme} />,
    )

    const el = testRenderer.toJSON()
    expect(el).toHaveStyleRule('opacity', '0')
  })

  it('should mount AuthButton', () => {
    const testRenderer = TestRenderer.create(
      <AuthButton theme={theme} />,
    )

    const el = testRenderer.toJSON()
    expect(el).toHaveStyleRule('border', '1px solid #FFF')
    expect(el).toHaveStyleRule('color', theme.colors.white)
  })
})
